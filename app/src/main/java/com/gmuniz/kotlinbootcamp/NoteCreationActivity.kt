package com.gmuniz.kotlinbootcamp

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.gmuniz.kotlinbootcamp.model.Note
import kotlinx.android.synthetic.main.activity_note_creation.*
import kotlinx.android.synthetic.main.content_note_creation.*

class NoteCreationActivity : AppCompatActivity() {

    companion object {
        const val REQUEST_CODE = 13145
        const val EXTRA_NOTE = "com.gmuniz.NoteCreationActivity.EXTRA_NOTE"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_creation)
        setSupportActionBar(toolbar)

        fab.setOnClickListener {
            val intent = Intent()
            intent.putExtra(EXTRA_NOTE, Note(title_input.text.toString(), description_input.text.toString()))
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
}