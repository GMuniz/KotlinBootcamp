package com.gmuniz.kotlinbootcamp.model

import java.io.Serializable
import java.util.*

data class Note(val title: String, val description: String, val date: Calendar = Calendar.getInstance()) : Serializable