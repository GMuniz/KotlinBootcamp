package com.gmuniz.kotlinbootcamp

import android.app.Activity
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.gmuniz.kotlinbootcamp.model.Note
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: ListViewModel
    private lateinit var adapter: NotesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        fab.setOnClickListener {
            val intent = Intent(this, NoteCreationActivity::class.java)
            startActivityForResult(intent, NoteCreationActivity.REQUEST_CODE)
        }

        viewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)

        adapter = NotesAdapter(viewModel.notes) { note -> onNoteClick(note) }
        notes_recyclerview.adapter = adapter

        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL

        notes_recyclerview.layoutManager = linearLayoutManager

        val itemDecoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        notes_recyclerview.addItemDecoration(itemDecoration)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == NoteCreationActivity.REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val note = data?.getSerializableExtra(NoteCreationActivity.EXTRA_NOTE) as Note
            viewModel.addNote(note)
            adapter.notifyDataSetChanged()
        }
    }


    private fun onNoteClick(note: Note) {
        val intent = Intent(this, NoteDetailActivity::class.java)
        intent.putExtra(NoteDetailActivity.EXTRA_NOTE, note)
        startActivity(intent)
    }
}
