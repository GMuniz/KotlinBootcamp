package com.gmuniz.kotlinbootcamp

import android.arch.lifecycle.ViewModel
import com.gmuniz.kotlinbootcamp.model.Note

data class ListViewModel(var notes: MutableList<Note> = ArrayList()) : ViewModel() {

    fun addNote(note: Note) {
        notes.add(note)
    }
}