package com.gmuniz.kotlinbootcamp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.gmuniz.kotlinbootcamp.model.Note
import kotlinx.android.synthetic.main.activity_note_detail.*
import java.text.DateFormat

class NoteDetailActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_NOTE = "com.gmuniz.NoteDetailActivity.EXTRA_NOTE"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_detail)

        val note = intent.getSerializableExtra(EXTRA_NOTE) as Note

        val formatter = DateFormat.getDateInstance(DateFormat.FULL)
        formatter.timeZone = note.date.timeZone

        note_description.text = note.description
        note_creation_date.text = "Creado el " + formatter.format(note.date.time)
    }
}
