package com.gmuniz.kotlinbootcamp

import android.support.v7.widget.RecyclerView
import android.view.View
import com.gmuniz.kotlinbootcamp.model.Note
import kotlinx.android.synthetic.main.item_note.view.*
import java.text.DateFormat

class NoteViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    fun bind(note: Note, onClick: (note: Note) -> Unit) {
        itemView.note_title.text = note.title

        val formatter = DateFormat.getDateInstance(DateFormat.LONG)
        formatter.timeZone = note.date.timeZone
        itemView.note_creation_date.text = formatter.format(note.date.time)
        itemView.setOnClickListener {
            onClick.invoke(note)
        }
    }
}